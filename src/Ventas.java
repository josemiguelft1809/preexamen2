/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class Ventas {
    
    private int codigoVenta;
    private float cantidad;
    private int tipo;
    private float precio;
    
    public Ventas(){
        this.codigoVenta =0;
        this.cantidad=0.0f;
        this.tipo=0;
        this.precio=0.0f;
    }

    public Ventas(int codigoVenta, float cantidad, int tipo, float precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Ventas(Ventas x){
        this.codigoVenta=x.codigoVenta;
        this.cantidad=x.cantidad;
        this.tipo=x.tipo;
        this.precio=x.precio;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public float calcularCostoVenta(){
        float costoVenta = 0.0f;
        if (this.tipo==0){
            costoVenta =(float) (this.cantidad * 17.50);
            
        }
        if (this.tipo==1){
            costoVenta =(float) (this.cantidad * 18.50);
            
        }
        return costoVenta;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto= (float) (this.calcularCostoVenta() * 0.16);
        return impuesto;
    }
    public float calcularTotal(){
        float total = 0.0f;
        total= this.calcularCostoVenta() + this.calcularImpuesto();
        return total;
    }
    
}
